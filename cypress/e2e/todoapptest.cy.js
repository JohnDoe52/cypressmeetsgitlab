describe("Testing the todo app", () => {
    beforeEach(() => {
        cy.visit('https://localhost:3000');
        cy.viewport(1920, 1080);
    });

    it("add todo", () => {
          cy.wait(500);
          cy.contains("Click here to login").wait(500).click();
          cy.get("#title").type("the nunt").type("{enter}");
          cy.get("#title").type("the other nunt").type("{enter}");
          cy.contains("the nunt");
          cy.contains("the other nunt");
    });

    it("count todo", () => {
        cy.wait(500);
        cy.contains("Click here to login").wait(500).click();
        cy.get("#title").type("the nunt").type("{enter}");
        cy.get("#title").type("the other nunt").type("{enter}");
        cy.contains("Total Todos: 2");
    });

    it("check todo", () => {
        cy.wait(500);
        cy.contains("Click here to login").wait(500).click();
        cy.get("#title").type("the nunt").type("{enter}");
        cy.get("[type='checkbox']").check();
        cy.contains("Total Todos: 1");
    });
}); 